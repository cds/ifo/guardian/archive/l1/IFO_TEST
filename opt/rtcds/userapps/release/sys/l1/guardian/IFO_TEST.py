# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
from guardian import GuardState, GuardStateDecorator
from guardian.ligopath import userapps_path
from subprocess import Popen, PIPE,check_output, call
import cdsutils
#from collections import namedtuple
import time
import math
import sys

sys.path.append('/ligo/cdscfg')
import stdenv as cds;

#sys.path.append('/opt/rtcds/userapps/release/isc/l1/guardian')
#import isclib.matrices as matrix

#cds.INIT_ENV()

##################################################
# SYSTEM PARAMETERS
# should be located in:
# sys/<ifo>/guardian/ifoconfig.py
#import ifoconfig

##################################################

#from ifolib import down

# The down scripts to be run in case of lock loss
#down_script_file = cds.USERAPPS + '/lsc/l1/scripts/autolock/drfpmi/l1_down'
#yarm_down_script_file = cds.USERAPPS + '/lsc/l1/scripts/autolock/drfpmi/l1_yarm_down.sh'

# the other scripts

#clear_oaf_script_file = '/home/anamaria.effler/scripts/oaf_clear_hist.py'
#psl_power_down_script_file = cds.USERAPPS + '/lsc/' + cds.ifo + '/scripts/transition/als/pslpowerdown.py'
#aligner_script_file = cds.USERAPPS + '/sus/common/scripts/aligner.py'

# initial request on initialization
request = 'IDLE'

class IDLE(GuardState):
    goto = True

    def main(self):
        pass 
    def run(self):
        notify('MC not locked: cannot leave IDLE state')
        return True


class INIT(GuardState):
    def run(self):
        log(ezca['LSC-DARM_OUTPUT'])
        return True

##################################################

edges = [
    ('INIT','IDLE'),
    ('IDLE','INIT')]
#ALS EDGES
# SVN $Id$
# $HeadURL$
             
            
